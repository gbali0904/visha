package com.chating.app;


import android.app.Application;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.database.sqlite.SQLiteDatabase;
import android.preference.PreferenceManager;
import android.view.Gravity;
import android.view.Window;
import android.view.WindowManager;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.FirebaseDatabase;
import com.google.gson.Gson;
import com.rey.material.widget.ProgressView;

import java.text.DateFormatSymbols;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import rx.Scheduler;
import rx.schedulers.Schedulers;


public class AppController extends Application {

    public static final String TAG = AppController.class
            .getSimpleName();
    private static final String PREFS_CHAT = "com.chating.app";
    private static final String PASSWORD_PATTERN = "^.{8,}$";

    private static final String MOBILE_PATTERN = "[0-9]{10}";

    private static final String EMAIL_PATTERN = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";
    private static AppController mInstance;
    private SharedPreferences pref;
    private FirebaseAuth mAuth;


    public static synchronized AppController getInstance() {

        return mInstance;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        pref = PreferenceManager.getDefaultSharedPreferences(AppController.this);
        mInstance = this;
        mAuth = FirebaseAuth.getInstance();
        FirebaseDatabase.getInstance().setPersistenceEnabled(true);
    }

    public SharedPreferences getPreference() {
        if (pref == null)
            pref = getSharedPreferences(PREFS_CHAT ,Context.MODE_PRIVATE);
        return pref;
    }
    public SharedPreferences.Editor getPreferenceEditor() {
        if (pref == null)
            pref = getSharedPreferences(PREFS_CHAT ,Context.MODE_PRIVATE);
        return pref.edit();
    }


    public Boolean passwordValidator(String password){
        Pattern pattern = Pattern.compile(PASSWORD_PATTERN);
        Matcher matcher = pattern.matcher(password);
        return matcher.matches();
    }


    public Boolean mobileNoValidator(String mobile_no){
        Pattern pattern = Pattern.compile(MOBILE_PATTERN);
        Matcher matcher = pattern.matcher(mobile_no);
        return matcher.matches();
    }

    public Boolean emailValidator(String email){
        Pattern pattern = Pattern.compile(EMAIL_PATTERN);
        Matcher matcher = pattern.matcher(email);
        return matcher.matches();
    }



}
