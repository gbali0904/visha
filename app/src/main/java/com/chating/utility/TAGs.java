package com.chating.utility;

public class TAGs {
    public static final String ID = "id";
    public static final String LOGIN_STATUS = "login_sattus";
    public static final String NAME = "name";
    public static final String EMAIL = "email";
    public static final String PHONE = "phone";
    public static final String IMAGE = "image";


    public static final String VERSION_NAME = "version_name";
    public static final String GCM_ID = "gcm_id";
    public static final String FIREBASE_URL ="https://fcm.googleapis.com/fcm/send" ;
    public static String BASE_URL="http://b145fb4a.ngrok.io/LegalIQ/";
    //public static String BASE_URL="http://34.215.99.251:8080/LegalIQ/";
    public static String DEVICE_TYPE="Android";
    public static String DEVICE_MODEL="device_model";
    public static String OS_VERSION="os_version";

    // broadcast receiver intent filters
    public static final String REGISTRATION_COMPLETE = "registrationComplete";
    public static final String PUSH_NOTIFICATION = "pushNotification";

    // id to handle the notification in the notification tray
    public static final int NOTIFICATION_ID = 100;
    public static final int NOTIFICATION_ID_BIG_IMAGE = 101;
    public static final String SHARED_PREF = "ah_firebase";
}

