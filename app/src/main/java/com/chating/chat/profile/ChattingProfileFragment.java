package com.chating.chat.profile;


import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.chating.R;
import com.chating.account.LoginActivity;
import com.chating.app.AppController;
import com.chating.splash.SplashActivity;
import com.chating.utility.TAGs;
import com.mikhaellopez.circularimageview.CircularImageView;
import com.squareup.picasso.Picasso;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

public class ChattingProfileFragment extends Fragment {
    @BindView(R.id.imageProfile)
    CircularImageView imageProfile;
    @BindView(R.id.name)
    TextView name;
    @BindView(R.id.editemailid)
    TextView editemailid;
    @BindView(R.id.editmobileNo)
    TextView editmobileNo;
    Unbinder unbinder;
    @BindView(R.id.logout)
    TextView logout;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_chatting_profile, container, false);
        unbinder = ButterKnife.bind(this, view);

        name.setText("" + AppController.getInstance().getPreference().getString(TAGs.NAME, ""));
        editemailid.setText("" + AppController.getInstance().getPreference().getString(TAGs.EMAIL, ""));
        editmobileNo.setText("" + AppController.getInstance().getPreference().getString(TAGs.PHONE, ""));
        String img_url = AppController.getInstance().getPreference().getString(TAGs.IMAGE, "");

        if (img_url.equals("")) {
            Picasso.get().load(R.mipmap.ic_launcher_round).into(imageProfile);
        } else {
            Picasso.get().load(img_url).into(imageProfile);
        }

        return view;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @OnClick(R.id.logout)
    public void onViewClicked() {
        AppController.getInstance().getPreferenceEditor().putBoolean(TAGs.LOGIN_STATUS, false).commit();
        AppController.getInstance().getPreferenceEditor().putString(TAGs.ID, "").commit();
        AppController.getInstance().getPreferenceEditor().putString(TAGs.NAME,"").commit();
        AppController.getInstance().getPreferenceEditor().putString(TAGs.EMAIL,"").commit();
        AppController.getInstance().getPreferenceEditor().putString(TAGs.PHONE,"").commit();
        AppController.getInstance().getPreferenceEditor().putString(TAGs.IMAGE, "").commit();
        startActivity(new Intent(getActivity(), LoginActivity.class));
        getActivity().finish();
    }
}
