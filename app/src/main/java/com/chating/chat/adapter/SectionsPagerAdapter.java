package com.chating.chat.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.chating.chat.chatting.ChatsFragment;
import com.chating.chat.friends.FriendsFragment;
import com.chating.chat.profile.ChattingProfileFragment;

public class SectionsPagerAdapter extends FragmentPagerAdapter {


    public SectionsPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {

        switch(position) {
            case 0:
                ChatsFragment chatsFragment = new ChatsFragment();
                return  chatsFragment;
            case 1:
                FriendsFragment friendsFragment = new FriendsFragment();
                return friendsFragment;

            case 2:
                ChattingProfileFragment chattingProfileFragment = new ChattingProfileFragment();
                return chattingProfileFragment;

            default:
                return null;
        }

    }

    @Override
    public int getCount() {
        return 3;
    }

    public CharSequence getPageTitle(int position){
        switch (position) {
            case 0:
                return "CHATS";

            case 1:
                return "FRIENDS";

            case 2:
                return "PROFILE";

            default:
                return null;
        }

    }

}
