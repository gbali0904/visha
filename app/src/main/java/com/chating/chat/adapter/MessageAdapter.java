package com.chating.chat.adapter;

import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.chating.R;
import com.chating.app.AppController;
import com.chating.chat.messages.model.Messages;
import com.chating.utility.TAGs;
import com.chating.utility.Tools;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.squareup.picasso.Picasso;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;


public class MessageAdapter extends RecyclerView.Adapter<MessageAdapter.MessageViewHolder> {
    private final Context messagesActivityClass;
    private List<Messages> mMessageList;
    private DatabaseReference mUserDatabase;

    public MessageAdapter(List<Messages> mMessageList, Context messagesActivityClass) {

        this.mMessageList = mMessageList;
        this.messagesActivityClass=messagesActivityClass;

    }

    @Override
    public MessageViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.message_single_layout, parent, false);

        return new MessageViewHolder(v);

    }

    public class MessageViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.name)
        TextView name;
        @BindView(R.id.message_text_layout)
        TextView messageTextLayout;
        @BindView(R.id.text_time)
        TextView textTime;
        @BindView(R.id.lyt_thread)
        CardView lyt_thread;
        @BindView(R.id.image)
        ImageView image;
        @BindView(R.id.lyt_parent)
        LinearLayout lyt_parent;

        public MessageViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }

    @Override
    public void onBindViewHolder(final MessageViewHolder viewHolder, int i) {
        Messages c = mMessageList.get(i);
        //ModelForLogin modelForLogin = AppController.getInstance().getGson().fromJson(AppController.getInstance().getPreference().getString(TAGs.LOGIN_JSON_DATA, ""), ModelForLogin.class);

        String from_user = c.getFrom();
        String message_type = c.getType();
        mUserDatabase = FirebaseDatabase.getInstance().getReference().child("Users").child(from_user);
        mUserDatabase.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                String name = dataSnapshot.child("name").getValue().toString();
                viewHolder.name.setText(name);
            }
            @Override
            public void onCancelled(DatabaseError databaseError) {
            }
        });

        Log.e("this","From ID ::"+from_user);

        if(!from_user.equals(AppController.getInstance().getPreference().getString(TAGs.ID,""))){
            viewHolder.lyt_parent.setPadding(15, 10, 100, 10);
            viewHolder.lyt_parent.setGravity(Gravity.LEFT);
            viewHolder.lyt_thread.setCardBackgroundColor(Color.parseColor("#FFFFFF"));
        }else{
            viewHolder.lyt_parent.setPadding(100, 10, 15, 10);
            viewHolder.lyt_parent.setGravity(Gravity.RIGHT);
            viewHolder.lyt_thread.setCardBackgroundColor(Color.parseColor("#FFE0B2"));
        }
        if (message_type.equals("text")) {
            viewHolder.messageTextLayout.setVisibility(View.VISIBLE);
            viewHolder.image.setVisibility(View.GONE);
            viewHolder.messageTextLayout.setText(c.getMessage());
            String time = getReadableTime(c.getTime());
            Log.e("this","time::"+time +"message ::"+c.getMessage());
            viewHolder.textTime.setText(time);
        } else {
            viewHolder.messageTextLayout.setVisibility(View.GONE);
            viewHolder.image.setVisibility(View.VISIBLE);
            Picasso.get().load(c.getMessage()).into(viewHolder.image);
            String time = getReadableTime(c.getTime());
            Log.e("this","time::"+time);
            viewHolder.textTime.setText(time);
        }
    }
    public String getReadableTime(long time)
    {
        try {
            return Tools.formatTime(Long.valueOf(time));
        }
        catch (NumberFormatException ignored) {
            return null;
        }
    }
    @Override
    public int getItemCount() {
        return mMessageList.size();
    }


}
