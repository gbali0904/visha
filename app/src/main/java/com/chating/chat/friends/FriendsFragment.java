package com.chating.chat.friends;


import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.chating.R;
import com.chating.app.AppController;
import com.chating.chat.friends.model.Friends;
import com.chating.chat.messages.MessagesActivity;
import com.chating.utility.TAGs;
import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.mikhaellopez.circularimageview.CircularImageView;
import com.squareup.picasso.Picasso;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;


public class FriendsFragment extends Fragment {

    public static final String TAG = FriendsFragment.class.getSimpleName();
    @BindView(R.id.friends_list)
    RecyclerView friendsList;
    Unbinder unbinder;
    @BindView(R.id.progressBar)
    ProgressBar progressBar;
    private RecyclerView mFriendsList;
    private DatabaseReference mUsersDatabase;
    private FirebaseAuth mAuth;
    private View mMainView;
    private String id;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             final Bundle savedInstanceState) {




        mMainView = inflater.inflate(R.layout.fragment_friends, container, false);

        mFriendsList = (RecyclerView) mMainView.findViewById(R.id.friends_list);
        mAuth = FirebaseAuth.getInstance();
        id = AppController.getInstance().getPreference().getString(TAGs.ID, "");
        mUsersDatabase = FirebaseDatabase.getInstance().getReference().child("Users");
        mUsersDatabase.keepSynced(true);
        mFriendsList.setHasFixedSize(true);
        mFriendsList.setLayoutManager(new LinearLayoutManager(getContext()));
        unbinder = ButterKnife.bind(this, mMainView);
        return mMainView;
    }


    @Override
    public void onStart() {
        super.onStart();
        FirebaseRecyclerAdapter<Friends, FriendsViewHolder> friendsRecyclerViewAdapter = new FirebaseRecyclerAdapter<Friends, FriendsViewHolder>(Friends.class, R.layout.users_single_layout, FriendsViewHolder.class, mUsersDatabase) {
            @Override
            protected void populateViewHolder(final FriendsViewHolder friendsViewHolder, Friends friends, int i) {
                progressBar.setVisibility(View.GONE);
                final String list_user_id = getRef(i).getKey();


                if (id.equals(list_user_id)) {
                    friendsViewHolder.setLayout(true);
                } else {
                    friendsViewHolder.setLayout(false);
                    Log.e("this", "list_user_id:: " + list_user_id);
                    mUsersDatabase.child(list_user_id).addValueEventListener(new ValueEventListener() {
                        @Override
                        public void onDataChange(DataSnapshot dataSnapshot) {

                            final String userName = dataSnapshot.child("name").getValue().toString();
                            final String userImage = dataSnapshot.child("image").getValue().toString();
                            final String device_token = dataSnapshot.child("device_token").getValue().toString();

                            friendsViewHolder.setName(userName);
                            friendsViewHolder.setImage(userImage);
                            friendsViewHolder.mView.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    Intent chatIntent = new Intent(getContext(), MessagesActivity.class);
                                    chatIntent.putExtra("user_id", list_user_id);
                                    chatIntent.putExtra("user_name", userName);
                                    chatIntent.putExtra("device_token", device_token);
                                    getActivity().startActivity(chatIntent);

                                }
                            });
                        }

                        @Override
                        public void onCancelled(DatabaseError databaseError) {

                        }
                    });
                }
            }
        };
        mFriendsList.setAdapter(friendsRecyclerViewAdapter);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }


    public static class FriendsViewHolder extends RecyclerView.ViewHolder {
        View mView;

        public FriendsViewHolder(View itemView) {
            super(itemView);
            mView = itemView;
        }

        public void setName(String name) {
            TextView userNameView = (TextView) mView.findViewById(R.id.user_single_name);
            userNameView.setText(name);
        }

        public void setImage(String image) {
            CircularImageView userImageView = (CircularImageView) mView.findViewById(R.id.user_single_image);
            if (image.equals("")) {
                Picasso.get().load(R.mipmap.ic_launcher_round).into(userImageView);
            } else {
                Picasso.get().load(image).into(userImageView);
            }
        }

        public void setLayout(boolean b) {
            LinearLayout linearLayout = (LinearLayout) mView.findViewById(R.id.layouut);
            if (b) {
                linearLayout.setVisibility(View.GONE);
            } else {
                linearLayout.setVisibility(View.VISIBLE);
            }

        }

    }


}
