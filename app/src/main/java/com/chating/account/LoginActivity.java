package com.chating.account;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.chating.R;
import com.chating.app.AppController;
import com.chating.chat.ChatActivity;
import com.chating.utility.TAGs;
import com.google.firebase.FirebaseError;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.gson.Gson;
import com.google.gson.JsonElement;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class LoginActivity extends AppCompatActivity {

    FirebaseAuth firebaseAuth;

    @BindView(R.id.LoginEmail)
    EditText LoginEmail;
    @BindView(R.id.Loginpassword)
    EditText Loginpassword;
    @BindView(R.id.login)
    Button login;
    @BindView(R.id.forgot)
    TextView forgot;
    @BindView(R.id.signuppage)
    TextView signuppage;
    @BindView(R.id.progressBar)
    ProgressBar progressBar;
    @BindView(R.id.content_main)
    RelativeLayout contentMain;
    private DatabaseReference postRef;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        postRef = FirebaseDatabase.getInstance().getReference().child("Users");
    }

    @OnClick({R.id.login, R.id.forgot, R.id.signuppage})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.login:
                final String email = LoginEmail.getText().toString().trim();
                String Pass = Loginpassword.getText().toString().trim();
                postRef.addListenerForSingleValueEvent(new ValueEventListener() {
                                 @Override
                                 public void onDataChange(DataSnapshot dataSnapshot) {
                                         Iterable<DataSnapshot> children = dataSnapshot.getChildren();
                                         for (DataSnapshot dataSnapshot1:children){
                                             if (dataSnapshot1.getKey().equalsIgnoreCase(email)) {
                                                 String value =new Gson().toJson(dataSnapshot1.getValue());
                                                 ModelForLogin modelForLogin = new Gson().fromJson( value, ModelForLogin.class);
                                                 if (modelForLogin.getPhone().equalsIgnoreCase(email)){
                                                     AppController.getInstance().getPreferenceEditor().putBoolean(TAGs.LOGIN_STATUS, true).commit();
                                                     AppController.getInstance().getPreferenceEditor().putString(TAGs.ID, dataSnapshot1.getKey()).commit();
                                                     AppController.getInstance().getPreferenceEditor().putString(TAGs.NAME, modelForLogin.getName()).commit();
                                                     AppController.getInstance().getPreferenceEditor().putString(TAGs.EMAIL, modelForLogin.getEmail()).commit();
                                                     AppController.getInstance().getPreferenceEditor().putString(TAGs.PHONE, modelForLogin.getPhone()).commit();
                                                     AppController.getInstance().getPreferenceEditor().putString(TAGs.IMAGE, modelForLogin.getImage()).commit();
                                                     startActivity(new Intent(LoginActivity.this, ChatActivity.class));
                                                     finish();
                                                 }else {
                                                     AppController.getInstance().getPreferenceEditor().putBoolean(TAGs.LOGIN_STATUS, false).commit();
                                                     AppController.getInstance().getPreferenceEditor().putString(TAGs.ID, "").commit();
                                                     AppController.getInstance().getPreferenceEditor().putString(TAGs.NAME,"").commit();
                                                     AppController.getInstance().getPreferenceEditor().putString(TAGs.EMAIL,"").commit();
                                                     AppController.getInstance().getPreferenceEditor().putString(TAGs.PHONE,"").commit();
                                                     AppController.getInstance().getPreferenceEditor().putString(TAGs.IMAGE, "").commit();
                                                     Toast.makeText(getApplicationContext(),"NO DATA FOUND",Toast.LENGTH_LONG).show();
                                                 }
                                             }
                                         }
                                 }
                                 @Override
                                  public void onCancelled(@NonNull DatabaseError databaseError) {
                                     AppController.getInstance().getPreferenceEditor().putBoolean(TAGs.LOGIN_STATUS, false).commit();
                                     AppController.getInstance().getPreferenceEditor().putString(TAGs.NAME,"").commit();
                                     AppController.getInstance().getPreferenceEditor().putString(TAGs.EMAIL,"").commit();
                                     AppController.getInstance().getPreferenceEditor().putString(TAGs.PHONE,"").commit();
                                     AppController.getInstance().getPreferenceEditor().putString(TAGs.IMAGE, "").commit();
                                     Toast.makeText(getApplicationContext(),"NO DATA FOUND",Toast.LENGTH_LONG).show();

                                     Toast.makeText(getApplicationContext(),databaseError.getMessage(),Toast.LENGTH_LONG).show();
                                 }
                        });


                break;
            case R.id.forgot:
                break;
            case R.id.signuppage:
                Intent i = new Intent(LoginActivity.this, SignUpActivity.class);
                startActivity(i);
                break;
        }
    }
}
