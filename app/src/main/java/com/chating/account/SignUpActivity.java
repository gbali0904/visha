package com.chating.account;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.chating.R;
import com.chating.app.AppController;
import com.chating.chat.ChatActivity;
import com.chating.utility.TAGs;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.iid.FirebaseInstanceId;

import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class SignUpActivity extends AppCompatActivity {
    @BindView(R.id.edtLoginEmail)
    EditText edtLoginEmail;
    @BindView(R.id.edtLoginpassword)
    EditText edtLoginpassword;
    @BindView(R.id.btnlogin)
    Button btnlogin;
    @BindView(R.id.content_main)
    RelativeLayout contentMain;
    @BindView(R.id.edtLoginName)
    EditText edtLoginName;
    @BindView(R.id.edtLoginMobile)
    EditText edtLoginMobile;
    @BindView(R.id.signuppage)
    TextView signuppage;
    @BindView(R.id.progressBar)
    ProgressBar progressBar;
    private FirebaseAuth mAuth;
    private String loginId;
    private String password;
    private DatabaseReference mUserDatabase;
    private String name;
    private String number;
    private String id;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);
        mAuth = FirebaseAuth.getInstance();
    }


    private void setFireBaseConstantUser(final String loginId, String password, final String name, final String number) {
        mUserDatabase = FirebaseDatabase.getInstance().getReference().child("Users").child(AppController.getInstance().getPreference().getString(TAGs.ID, ""));
        //device tocken id
        String device_token = FirebaseInstanceId.getInstance().getToken();
        HashMap<String, String> userMap = new HashMap<>();
        userMap.put("name", name);
        userMap.put("email", loginId);
        userMap.put("phone", number);
        userMap.put("image", "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTlkaSbo2TCG8DIiEtyKIPy590GgDHZvFLmfjPuUPCp8NIa7dZh");
        userMap.put("id", AppController.getInstance().getPreference().getString(TAGs.ID, ""));
        userMap.put("device_token", device_token);
        mUserDatabase.setValue(userMap).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                if (task.isSuccessful()) {
                    AppController.getInstance().getPreferenceEditor().putBoolean(TAGs.LOGIN_STATUS, true).commit();
                    AppController.getInstance().getPreferenceEditor().putString(TAGs.NAME, name).commit();
                    AppController.getInstance().getPreferenceEditor().putString(TAGs.EMAIL, loginId).commit();
                    AppController.getInstance().getPreferenceEditor().putString(TAGs.PHONE, number).commit();
                    AppController.getInstance().getPreferenceEditor().putString(TAGs.IMAGE, "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTlkaSbo2TCG8DIiEtyKIPy590GgDHZvFLmfjPuUPCp8NIa7dZh").commit();
                    startActivity(new Intent(SignUpActivity.this, ChatActivity.class));
                } else {
                    if (task.getException().getMessage().equalsIgnoreCase("The email address is already in use by another account.")) {
                       // startActivity(new Intent(SignUpActivity.this, ChatActivity.class));
                        AppController.getInstance().getPreferenceEditor().putBoolean(TAGs.LOGIN_STATUS, false).commit();
                        AppController.getInstance().getPreferenceEditor().putString(TAGs.NAME,"").commit();
                        AppController.getInstance().getPreferenceEditor().putString(TAGs.EMAIL,"").commit();
                        AppController.getInstance().getPreferenceEditor().putString(TAGs.PHONE,"").commit();
                        AppController.getInstance().getPreferenceEditor().putString(TAGs.IMAGE, "").commit();
                        Toast.makeText(getApplicationContext(),"The number/email is already in use by another account.",Toast.LENGTH_LONG).show();

                    } else {
                        Log.w("this", "createUserWithEmail:failure", task.getException());
                        Toast.makeText(SignUpActivity.this, "Authentication failed.", Toast.LENGTH_SHORT).show();
                    }
                }


            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                Log.e("this", "FAIL:::" + e.getMessage());
            }
        });
    }

    @OnClick({R.id.btnlogin, R.id.signuppage})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.btnlogin:
                loginId = edtLoginEmail.getText().toString().trim();
                name = edtLoginName.getText().toString().trim();
                number = edtLoginMobile.getText().toString().trim();
                password = edtLoginpassword.getText().toString().trim();
                //UUNIQE ID
                AppController.getInstance().getPreferenceEditor().putString(TAGs.ID, number).commit();
                setFireBaseConstantUser(loginId, password, name, number);
                break;
            case R.id.signuppage:
                Intent intent = new Intent(SignUpActivity.this, LoginActivity.class);
                startActivity(intent);
                break;
        }
    }
}
