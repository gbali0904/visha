package com.chating.splash;

import android.Manifest;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;

import com.chating.R;
import com.chating.app.AppController;
import com.chating.chat.ChatActivity;
import com.chating.utility.TAGs;
import com.chating.account.LoginActivity;

import java.util.Timer;
import java.util.TimerTask;

import me.tankery.permission.PermissionRequestActivity;

public class SplashActivity extends AppCompatActivity {

    private static int Splash_TIME_DELEY = 1000;
    private static final int PERMISSIONS_REQUEST_READ_PHONE_STATE = 1;
    private static final String[] MUST_PERMISSIONS = new String[] {
            Manifest.permission.CAMERA,
            Manifest.permission.WRITE_EXTERNAL_STORAGE,
            Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.ACCESS_FINE_LOCATION,
            Manifest.permission.ACCESS_COARSE_LOCATION
    };
    private static final int REQUEST_CODE = 121;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        checkPermissionALL();
    }

    private void checkPermissionALL() {
        String message = "We need SD Card and location permissions for better experience.";
        PermissionRequestActivity.start(this, REQUEST_CODE,
                MUST_PERMISSIONS, message, message);
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_CODE) {
            if (resultCode == RESULT_OK) {
                nextActivity();
            } else {
                Log.e("this","resultCode::"+resultCode);
                checkPermissionALL();
            }
        }
    }

    public void nextActivity(){
        TimerTask task = new TimerTask() {
            @Override
            public void run() {

                if ( AppController.getInstance().getPreference().getBoolean(TAGs.LOGIN_STATUS,false)){
                    startActivity(new Intent(SplashActivity.this, ChatActivity.class));
                    finish();
                }
                else {
                    startActivity(new Intent(SplashActivity.this, LoginActivity.class));
                    finish();
                }
            }
        };
        Timer t = new Timer();
        t.schedule(task, Splash_TIME_DELEY);
    }
}
