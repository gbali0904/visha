package com.chating.notification;


import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.os.Build;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.chating.R;
import com.chating.splash.SplashActivity;
import com.chating.utility.NotificationUtils;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
/**
 * Created by Android on 6/1/2017.
 */
public class MyFirebaseMessagingService extends FirebaseMessagingService {
    private static final String TAG = MyFirebaseMessagingService.class.getSimpleName();

    private NotificationUtils notificationUtils;
    private Intent myIntent;

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        Log.e(TAG, "From: " + remoteMessage.getFrom());

        if (remoteMessage == null)
            return;

        // Check if message contains a notification payload.
        if (remoteMessage.getNotification() != null) {
            Log.e(TAG, "Notification Body: " + remoteMessage.getNotification().getBody() +"\ntitle"+remoteMessage.getNotification().getTitle());
            String title = remoteMessage.getNotification().getTitle();
            handleNotification(remoteMessage.getNotification().getBody(),title);
        }
        // Check if message contains a data payload.
        if (remoteMessage.getData().size() > 0) {
            String body = remoteMessage.getData().get("message");
            String title = remoteMessage.getData().get("title");
            //Log.e("this",body);
            System.out.println(body);
            handleNotification(body,title);
        }
    }

    private void handleNotification(String message, String title) {
        if (!NotificationUtils.isAppIsInBackground(getApplicationContext())) {
            // app is in foreground, broadcast the push message
            NotificationCompat.Builder builder = new  NotificationCompat.Builder(this)
                    .setSmallIcon(R.mipmap.ic_launcher)
                    .setContentTitle("Chating")
                    .setChannelId("7664")
                    .setPriority(NotificationCompat.PRIORITY_DEFAULT)
                    .setContentText(message);
            NotificationManager manager = (NotificationManager)     getSystemService(NOTIFICATION_SERVICE);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                CharSequence name = "Chating";
                String description = "message";
                int importance = NotificationManager.IMPORTANCE_DEFAULT;
                NotificationChannel channel = new NotificationChannel("7664", name, importance);
                channel.setDescription(description);
                manager.createNotificationChannel(channel);
            }
            manager.notify(0, builder.build());
        }else {
            if (title.equalsIgnoreCase("New Message"))
             myIntent = new Intent(this, SplashActivity.class);
            PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, myIntent, 0);
            NotificationCompat.Builder builder = new  NotificationCompat.Builder(this)
                    .setSmallIcon(R.mipmap.ic_launcher)
                    .setContentTitle("Chating")
                    .setContentText(message)
                    .setContentIntent(pendingIntent)
                    .setAutoCancel(true);
            NotificationManager manager = (NotificationManager)     getSystemService(NOTIFICATION_SERVICE);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                CharSequence name ="Chating";
                String description = "message";
                int importance = NotificationManager.IMPORTANCE_DEFAULT;
                NotificationChannel channel = new NotificationChannel("7664", name, importance);
                channel.setDescription(description);
                manager.createNotificationChannel(channel);
            }
            manager.notify(0, builder.build());

        }

    }


}